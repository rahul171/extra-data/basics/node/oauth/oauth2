const path = require('path');
const hbs = require('hbs');
const router = require('./routes');

const connection = require('./db/mongoose');

connection.connect();

const express = require('express');
const app = express();

app.set('view engine', 'hbs');
app.set('views', path.join(__dirname, 'views'));

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

router.configureRoutes(app);

app.use((err, req, res, next) => {
    res.status(400).send(err.message);
});

const port = process.env.PORT;

app.listen(port, () => {
    console.log(`listening on ${port}`);
});

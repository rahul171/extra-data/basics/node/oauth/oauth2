const User = require('../models/user');
const oauth = require('../oauth');

const save = async ({ emailAddress, tokens } = {}) => {
    if (!emailAddress) {
        return false;
    }

    // tokens = mapTokenFields(tokens);

    const user = new User({ email: emailAddress, tokens });
    return await user.save();
}

// const mapTokenFields = (tokens) => {
//     return {
//         accessToken: tokens.access_token,
//         refreshToken: tokens.refresh_token,
//         idToken: tokens.id_token,
//         tokenType: tokens.token_type,
//         expiryDate: tokens.expiry_date,
//     };
// }

const get = async (id) => {
    const query = {};

    if (id) {
        query._id = id;
    }

    // return {something:'someone'}

    console.log(id);
    console.log(query);

    return await User.find(query);
}

const getUserData = async (id) => {
    if (!id) {
        return false;
    }

    const user = await User.findById(id);

    console.log(user.tokens);

    oauth.setCredentials(user.tokens);

    return await oauth.getEmailAddress();
}

const deleteAll = async () => {
    return await User.deleteMany();
}

const getEmailIds = async (id) => {
    const user = await get(id);
    console.log(user);
    oauth.setCredentials(user[0].tokens);
    return oauth.getEmails();
}

const getEmails = async (id) => {
    const emails = await getEmailIds(id);
    let i = 0;
    for (const item of emails.data.messages) {
        if (i++ > 5) break;
        item.content = await oauth.gmail.users.messages.get({ userId: 'me', id: item.id });
    }
    return emails;
}

const oauth2GetUser = async (id) => {
    const dbUser = await get(id);
    oauth.setCredentials(dbUser[0].tokens);
    return oauth.oauth2GetUser();
}

module.exports = { save, get, getUserData, deleteAll, getEmails, oauth2GetUser };

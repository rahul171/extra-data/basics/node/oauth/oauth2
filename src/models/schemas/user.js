const { Schema } = require('mongoose');

const schema = new Schema({
    email: {
        type: String,
        required: true
    },
    tokens: {
        access_token: { type: String },
        refresh_token: { type: String },
        id_token: { type: String },
        token_type: { type: String },
        expiry_date: { type: String },
    }
});

module.exports = schema;

const { google } = require('googleapis');
const gmail = google.gmail('v1');
const oauth2 = google.oauth2('v2');

const oauth2Client = new google.auth.OAuth2(
    process.env.GOOGLE_OAUTH_CLIENT_ID,
    process.env.GOOGLE_OAUTH_CLIENT_SECRET,
    process.env.GOOGLE_OAUTH_REDIRECT_URL
);

oauth2Client.on('tokens', (tokens) => {
    console.log('----------tokens----------');
    console.log(tokens);
    if (tokens.refresh_token) {
        // store the refresh_token in my database!
        console.log(tokens.refresh_token);
    }
    console.log(tokens.access_token);
    console.log('----------end tokens----------');
});

google.options({ auth: oauth2Client });

const scopes = [
    'https://www.googleapis.com/auth/userinfo.email',
    'https://mail.google.com/'
];

const generateAuthUrl = () => {
    return oauth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: scopes
    });
}

const authenticate = async (code) => {
    const data = await getToken(code);
    console.log(data);
    const tokens = data.tokens;
    setCredentials(tokens);
    const emailAddress = await getEmailAddress();
    return { tokens, emailAddress };
}

const getToken = async (code) => {
    return await oauth2Client.getToken(code);
}

const getEmailAddress = async () => {
    const response = await gmail.users.getProfile({ userId: 'me' });
    // console.log(response);
    return response.data.emailAddress;
}

const setCredentials = (tokens) => {
    return oauth2Client.setCredentials(tokens);
}

const getEmails = async () => {
    const emails = await gmail.users.messages.list({ userId: 'me' });
    return emails;
}

const oauth2GetUser = async () => {
    const user = await oauth2.userinfo.get({});
    console.log(user);
    return user;
}

module.exports = { generateAuthUrl, authenticate, setCredentials, getEmailAddress, getEmails, oauth2Client, gmail, oauth2GetUser };

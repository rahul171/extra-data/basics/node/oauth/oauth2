const oauth = require('./oauth');
const oauthPassport = require('./oauth-passport');

const configureRoutes = (app) => {
    app.use('/users', oauth);
    app.use('/oauth-passport', oauthPassport);
};

module.exports = { configureRoutes };

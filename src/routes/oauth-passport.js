const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20').Strategy;

const router = require('express').Router();

passport.use(
    new GoogleStrategy(
        {
            clientID: process.env.GOOGLE_OAUTH_CLIENT_ID,
            clientSecret: process.env.GOOGLE_OAUTH_CLIENT_SECRET,
            callbackURL: 'http://localhost:3000/oauth-passport/verify'
        },
        (accessToken, refreshToken, profile, cb) => {
            console.log(accessToken);
            console.log(refreshToken);
            console.log(profile);
        }
    )
);

router.get('/url', passport.authenticate('google', { scope: ['profile', 'email'] }));
router.get('/verify', passport.authenticate('google'), (req, res) => {
    res.send('maybe');
});

module.exports = router;

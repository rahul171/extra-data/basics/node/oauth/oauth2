const oauth = require('../oauth');
const router = require('express').Router();
const UserComponent = require('../components/user');

router.get('/login', (req, res, next) => {
    const url = oauth.generateAuthUrl();
    res.render('url', { url });
});

router.get('/login/:id', async (req, res, next) => {
    const id = req.params.id;
    const userData = await UserComponent.getUserData(id);
    res.send(userData);
});

router.get('/verify', async (req, res, next) => {
    const code = req.query.code;

    const authData = await oauth.authenticate(code);
    const savedUser = await UserComponent.save(authData);

    res.send(savedUser);
});

router.get('/delete', async (req, res, next) => {
    const deleted = await UserComponent.deleteAll();
    res.send(deleted);
});

router.get('/all', async (req, res, next) => {
    const users = await UserComponent.get();
    res.send(users);
});

router.get('/:id', async (req, res, next) => {
    const users = await UserComponent.get(req.params.id);
    res.send(users);
});

router.get('/:id/emails', async (req, res, next) => {
    const emails = await UserComponent.getEmails(req.params.id);
    res.send(emails);
});

router.get('/:id/oauth2user', async (req, res, next) => {
    const user = await UserComponent.oauth2GetUser(req.params.id);
    res.send(user);
});

module.exports = router;
